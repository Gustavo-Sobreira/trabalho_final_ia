from flask import Flask, render_template, request, jsonify
from pymongo import MongoClient
import numpy as np
import random
import nltk
from nltk.stem import WordNetLemmatizer
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense, Dropout

app = Flask(__name__)

# Inicialização do NLTK
nltk.download('punkt')
nltk.download('wordnet')

# Inicialização do MongoDB
cliente = MongoClient('mongodb://root:root@localhost:27017')
banco_de_dados = cliente['GPTECO']
colecao_respostas = banco_de_dados['respostas']
colecao_feedback = banco_de_dados['feedback']

# Carregar dados do MongoDB
dados = {'intents': list(colecao_respostas.find({}, {'_id': False}))}

dados_feedback = {'intents': list(colecao_feedback.find({}, {'_id': False}))}

for feedback_intent in dados_feedback['intents']:
    for intent in dados['intents']:
        if intent['tag'] == feedback_intent['tag']:
            intent['patterns'].extend(feedback_intent['patterns'])
            intent['responses'].extend(feedback_intent['responses'])

# Pré-processamento de texto
lemmatizer = WordNetLemmatizer()

palavras = []
classes = []
documentos = []
ignorar_palavras = ['?', '!', '.', ',']

for intent in dados['intents']:
    for pattern in intent['patterns']:
        w = nltk.word_tokenize(pattern)
        palavras.extend(w)
        documentos.append((w, intent['tag']))
        if intent['tag'] not in classes:
            classes.append(intent['tag'])

palavras = [lemmatizer.lemmatize(w.lower()) for w in palavras if w not in ignorar_palavras]
palavras = sorted(list(set(palavras)))
classes = sorted(list(set(classes)))

treinamento = []
saida_vazia = [0] * len(classes)

for doc in documentos:
    bag = []
    palavras_padrao = doc[0]
    palavras_padrao = [lemmatizer.lemmatize(word.lower()) for word in palavras_padrao]
    for w in palavras:
        bag.append(1) if w in palavras_padrao else bag.append(0)
    
    saida_linha = list(saida_vazia)
    saida_linha[classes.index(doc[1])] = 1
    
    treinamento.append([bag, saida_linha])

if treinamento:  # Verificar se treinamento não está vazio
    train_x = np.array([np.array(t[0]) for t in treinamento])
    train_y = np.array([np.array(t[1]) for t in treinamento])

    modelo = Sequential()
    modelo.add(Dense(128, input_shape=(len(train_x[0]),), activation='relu'))
    modelo.add(Dropout(0.5))
    modelo.add(Dense(64, activation='relu'))
    modelo.add(Dropout(0.5))
    modelo.add(Dense(len(train_y[0]), activation='softmax'))

    modelo.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    def treinar_modelo():
        modelo.fit(train_x, train_y, epochs=200, batch_size=5, verbose=1)
        modelo.save('chatbot_model.keras')

    treinar_modelo()

def limpar_frase(frase):
    palavras_frase = nltk.word_tokenize(frase)
    palavras_frase = [lemmatizer.lemmatize(word.lower()) for word in palavras_frase]
    return palavras_frase

def bow(frase, palavras):
    palavras_frase = limpar_frase(frase)
    bag = [0] * len(palavras)
    for s in palavras_frase:
        for i, w in enumerate(palavras):
            if w == s:
                bag[i] = 1
    return np.array(bag)

def prever_classe(frase, modelo):
    p = bow(frase, palavras)
    res = modelo.predict(np.array([p]))[0]
    LIMIAR_ERRO = 0.25
    resultados = [[i, r] for i, r in enumerate(res) if r > LIMIAR_ERRO]
    resultados.sort(key=lambda x: x[1], reverse=True)
    lista_retorno = []
    for r in resultados:
        lista_retorno.append({"intent": classes[r[0]], "probabilidade": str(r[1])})
    return lista_retorno

def obter_resposta(ints, intents_json):
    if ints:
        tag = ints[0]['intent']
        lista_de_intents = intents_json['intents']
        for i in lista_de_intents:
            if i['tag'] == tag:
                result = random.choice(i['responses'])
                break
        return result
    else:
        return "Desculpe, não entendi. Pode reformular?"

def salvar_feedback(mensagem, resposta, e_util):
    novo_feedback = {
        "patterns": [mensagem],
        "responses": [resposta],
        "tag": "helpful" if e_util else "not_helpful"
    }
    colecao_feedback.insert_one(novo_feedback)

def resposta_chatbot(msg):
    if 'modelo' in globals():
        ints = prever_classe(msg, modelo)
        res = obter_resposta(ints, dados)
        return res
    else:
        return "Desculpe, ainda estou aprendendo. Por favor, tente novamente mais tarde."

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/chat', methods=['POST'])
def chat():
    user_message = request.form['user_message']
    bot_response = resposta_chatbot(user_message)
    return {'bot_response': bot_response}

@app.route('/feedback', methods=['POST'])
def feedback():
    data = request.json
    mensagem = data['message']
    resposta = data['feedback'] 
    e_util = resposta == 'helpful'
    print(e_util)
    salvar_feedback(mensagem, resposta, e_util)
    return jsonify({'success': True})



if __name__ == '__main__':
    app.run(debug=True)
