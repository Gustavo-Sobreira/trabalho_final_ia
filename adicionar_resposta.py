from pymongo import MongoClient

cliente = MongoClient('mongodb://root:root@localhost:27017')
banco_de_dados = cliente['GPTECO']
colecao_respostas = banco_de_dados['respostas']

def adicionar_resposta(tag, patterns, responses):
    resposta = {
        "tag": tag,
        "patterns": patterns,
        "responses": responses
    }
    
    colecao_respostas.insert_one(resposta)
    print("Resposta adicionada com sucesso!")

tag = input('Tag: ')
patterns = ["Padrão 1", "Padrão 2"]
responses = ["Resposta 1", "Resposta 2"]

adicionar_resposta(tag, patterns, responses)
